import express from "express";
export const router = express.Router();

export default {router};
//declarar primer ruta por omision

router.get('/',(req,res)=>{
    //parametros
    const params = {
        num_docente: req.query.num_docente,
        nombre: req.query.nombre,
        domicilio: req.query.domicilio,
        nivel: req.query.nivel,
        pago:req.query.pago,
        pagoHoraBase:req.query.pagoHoraBase,
        horas: req.query.horas,
        hijos: req.query.hijos
     
    };
res.render('recibodepago', params);
});
    
    
router.post('/recibodepago',(req,res)=>{
    const params = {
        num_docente: req.body.num_docente,
        nombre: req.body.nombre,
        domicilio: req.body.domicilio,
        nivel: req.body.nivel,
        pago:req.body.pago,
        pagoHoraBase:req.body.pagoHoraBase,
        horas: req.body.horas,
        hijos: req.body.hijos
    };
    res.render('recibodepago', params);
});